import React from 'react';
import ReactDOM from 'react-dom';

import './App.css';

import Swiper from 'swiper';
import SingleHolder from './projectHolder/singleHolder';
import ProjectHolder from './components/projectsHolder/ProjectsHolder';

class App extends React.Component {

  componentDidMount() {
    var swiperBottomScrollbarFull = new Swiper('.swiper-bottom-scrollbar-full', {
        allowTouchMove: true,
        slidesPerView: 'auto',
        grabCursor: true,
        preventClicks: true,
        spaceBetween: 30,
        keyboardControl: true,
        speed: 1000,
        pagination: {
            el: null
        },
        scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true,
            hide: false,
            snapOnRelease: true
        },
        mousewheel: {
            enable: true
        },
        keyboard: {
            enabled: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            767: {
                scrollbar: {
                    hide: true
                },
                spaceBetween: 0,
                autoHeight: true,
                centeredSlides: false,
                slidesOffsetAfter: 85
            }
        },
        on: {
            resize: function () {
                var windowWidth = window.width;
                console.log("swiper initialize");
                if(windowWidth <= 767){
                        swiperBottomScrollbarFull.direction('vertical');
                        swiperBottomScrollbarFull.detachEvents();
                }else{
                        swiperBottomScrollbarFull.direction('horizontal');
                        swiperBottomScrollbarFull.attachEvents();
                }
                swiperBottomScrollbarFull.update();
            }
        }
    });
  }
    

  render() {
    return (
      <section className="no-padding">
            <div className="swiper-bottom-scrollbar-full swiper-container">
              <ProjectHolder /> 
            </div>
        <div className="swiper-scrollbar"></div>
        
      </section>
    );
  }
};
export default App;
