import React from 'react';
import './singleHold.css';


const SingleHolder = () => {
	return (
            	<React.Fragment>
                    <div className="swiper-wrapper">
                        <div className="swiper-slide width-550px xs-width-100 xs-height-auto">
                            <div className="position-relative width-90 height-100 display-table padding-ten-all xs-padding-fifteen-all xs-width-100">
                                <div className="display-table-cell vertical-align-middle">
                                    <h4 className="text-medium-gray display-block margin-5px-bottom alt-font">Hello,</h4>
                                    <h6 className="text-medium-gray font-weight-300 margin-20px-bottom alt-font">I'm Colin Smith</h6>
                                    <p className="text-large dispaly-block pull-left font-weight-300 width-90 margin-35px-bottom">I design thoughtful digital experiences & beautiful brand aesthetics. I provide high quality web design services.</p>
                                    <img src="https://www.themezaa.com/html/pofo/images/signature.png" className="width-60 signature" alt=""/>
                                </div>
                            </div>
                        </div>
                   
                        <div className="swiper-slide width-auto xs-height-auto last-paragraph-no-margin">
                            <div className="height-100 display-table">
                                <div className="display-table-cell vertical-align-middle">
                                    <div className="display-block position-relative">
                                        <img src="https://www.themezaa.com/html/pofo/images/homepage-2-slider-img-9.jpg" alt=""/>
                                        <p className="bottom-text width-100 text-extra-small text-white text-uppercase text-center">Branding and Identity</p>
                                    </div>
                                    <div className="hover-title-box padding-55px-lr width-300px sm-width-100 sm-padding-20px-lr">
                                        <div className="separator width-50px bg-black md-display-none xs-margin-lr-auto"></div>
                                        <h3><a className="text-white font-weight-600 alt-font text-white-hover" href="single-project-page-01.html">Pixflow Studio</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
             
                        <div className="swiper-slide width-auto xs-height-auto last-paragraph-no-margin">
                            <div className="height-100 display-table">
                                <div className="display-table-cell vertical-align-middle">
                                    <div className="display-block position-relative">
                                        <img src="https://www.themezaa.com/html/pofo/images/homepage-2-slider-img-4.jpg" alt=""/>
                                        <p className="bottom-text width-100 text-extra-small text-white text-uppercase text-center">Branding and Brochure</p>
                                    </div>
                                    <div className="hover-title-box padding-55px-lr width-300px sm-width-100 sm-padding-20px-lr">
                                        <div className="separator width-50px bg-black md-display-none xs-margin-lr-auto"></div>
                                        <h3><a className="text-white font-weight-600 alt-font text-white-hover" href="single-project-page-02.html">Tailoring Interior</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                        <div className="swiper-slide width-auto xs-height-auto last-paragraph-no-margin">
                            <div className="height-100 display-table">
                                <div className="display-table-cell vertical-align-middle">
                                    <div className="display-block position-relative">
                                        <img src="https://www.themezaa.com/html/pofo/images/homepage-2-slider-img-10.jpg" alt=""/>
                                        <p className="bottom-text width-100 text-extra-small text-white text-uppercase text-center">Web and Photography</p>
                                    </div>
                                    <div className="hover-title-box padding-55px-lr width-300px sm-width-100 sm-padding-20px-lr">
                                        <div className="separator width-50px bg-black md-display-none xs-margin-lr-auto"></div>
                                        <h3><a className="text-white font-weight-600 alt-font text-white-hover" href="single-project-page-03.html">Digital Media</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                        <div className="swiper-slide width-auto xs-height-auto last-paragraph-no-margin">
                            <div className="height-100 display-table">
                                <div className="display-table-cell vertical-align-middle">
                                    <div className="display-block position-relative">
                                        <img src="https://www.themezaa.com/html/pofo/images/homepage-2-slider-img-12.jpg" alt=""/>
                                        <p className="bottom-text width-100 text-extra-small text-white text-uppercase text-center">Branding and Identity</p>
                                    </div>
                                    <div className="hover-title-box padding-55px-lr width-300px sm-width-100 sm-padding-20px-lr">
                                        <div className="separator width-50px bg-black md-display-none xs-margin-lr-auto"></div>
                                        <h3><a className="text-white font-weight-600 alt-font text-white-hover" href="single-project-page-04.html">HardDot Stone</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                        <div className="swiper-slide width-auto xs-height-auto last-paragraph-no-margin">
                            <div className="height-100 display-table">
                                <div className="display-table-cell vertical-align-middle">
                                    <div className="display-block position-relative">
                                        <img src="https://www.themezaa.com/html/pofo/images/homepage-2-slider-img-13.jpg" alt=""/>
                                        <p className="bottom-text width-100 text-extra-small text-white text-uppercase text-center">Web and Photography</p>
                                    </div>
                                    <div className="hover-title-box padding-55px-lr width-300px sm-width-100 sm-padding-20px-lr">
                                        <div className="separator width-50px bg-black md-display-none xs-margin-lr-auto"></div>
                                        <h3><a className="text-white font-weight-600 alt-font text-white-hover" href="single-project-page-05.html">Violator Series</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        <div className="swiper-slide width-auto xs-height-auto last-paragraph-no-margin">
                            <div className="height-100 display-table">
                                <div className="display-table-cell vertical-align-middle">
                                    <div className="display-block position-relative">
                                        <img src="https://www.themezaa.com/html/pofo/images/homepage-2-slider-img-11.jpg" alt=""/>
                                        <p className="bottom-text width-100 text-extra-small text-white text-uppercase text-center">Branding and Identity</p>
                                    </div>
                                    <div className="hover-title-box padding-55px-lr width-300px sm-width-100 sm-padding-20px-lr">
                                        <div className="separator width-50px bg-black md-display-none xs-margin-lr-auto"></div>
                                        <h3><a className="text-white font-weight-600 alt-font text-white-hover" href="single-project-page-01.html">Banana Design</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide width-150px xs-width-100 xs-height-auto">
                        </div>
                    </div>
                    <div className="swiper-scrollbar xs-display-none"></div>
                    <div className="swiper-pagination-vertical position-fixed bullet-deep-pink z-index-5"></div>
                </React.Fragment>
		);
};

export default SingleHolder;