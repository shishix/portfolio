import React from 'react';
import { NavLink } from 'react-router-dom';

const Menu = () => {
    return (
        <nav className="nav-wrapper">
            <ul className="cf">
                <li><NavLink to="/">HOME</NavLink></li>
                <li><NavLink to="/work">WORK</NavLink></li>
                <li><NavLink to="/profile">PROFILE</NavLink></li>
                <li><a href="http://www.google.com">BLOG</a></li>
            </ul>
        </nav>
    ); 
};

export default Menu;