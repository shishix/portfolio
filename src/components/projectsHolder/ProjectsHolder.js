import React from 'react';
import SelfIntro from '../selfIntro/SelfIntro';
import SingleProject from '../singleProject/SingleProject';

class ProjectHolder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showResume: false,
        };
        this.projectList = [
            {id: 0,
             pic: "https://static1.squarespace.com/static/503a2d03e4b01177339505ad/t/536e2001e4b0d04402a85b7a/1399726083702/?format=1000w",
             btmtxt: "First Project",
             introduction: "How to done"
            },
            {id: 1,
             pic: "https://static1.squarespace.com/static/503a2d03e4b01177339505ad/t/51b3bca6e4b0830c27f3a00a/1370733738962/LAI.jpg?format=1000w",
             btmtxt: "Second Project",
             introduction: "How to done"
            },
            {id: 2,
             pic: "https://static1.squarespace.com/static/503a2d03e4b01177339505ad/t/51c3769ae4b0998b1b51bb75/1371764381270/AllofUs.jpg?format=1000w",
             btmtxt: "Third Project",
             introduction: "How to done"
            },
            {id: 3,
             pic: "https://static1.squarespace.com/static/503a2d03e4b01177339505ad/t/51b3bcc1e4b0797e32fef067/1370733764969/mytimer.jpg?format=1000w",
             btmtxt: "Forth Project",
             introduction: "How to done"
            },
            {id: 4,
             pic: "https://static1.squarespace.com/static/503a2d03e4b01177339505ad/t/51b3bbe4e4b08305624e91ef/1370733542886/trillions.jpg?format=1000w",
             btmtxt: "Fifth Project",
             introduction: "How to done"
            },
        ];
        
    }

    checkResume = (e) => {
        const showresume = e;
        this.setState({
            showResume: showresume,
        })
    }
    render() {
        const { showResume } = this.state;
        return (
            <React.Fragment>
                    <div className="swiper-wrapper">
                        <SelfIntro resumeRender={(e) => this.checkResume(e)} showResume={showResume} />

                        {
                            !showResume ? this.projectList.map(item => (<SingleProject  key={item.id} project={item}/>)) : (<div>Resume</div>)
                        }
                        <div className="swiper-slide width-150px xs-width-100 xs-height-auto">
                        </div>
                    </div>
                    <div className="swiper-scrollbar xs-display-none"></div>
                    <div className="swiper-pagination-vertical position-fixed bullet-deep-pink z-index-5"></div>
                </React.Fragment>
        );
    }
}

export default ProjectHolder;