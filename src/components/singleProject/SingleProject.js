import React from 'react';


class SingleProject extends React.Component {

    render() {
        const { project } = this.props;
        return (
            <div className="swiper-slide width-auto xs-height-auto last-paragraph-no-margin">
                <div className="height-100 display-table">
                    <div className="display-table-cell vertical-align-middle">
                        <div className="display-block position-relative">
                            <img src={project.pic} alt=""/>
                            <p className="bottom-text width-100 text-extra-small text-white text-uppercase text-center">{project.btmtxt}</p>
                        </div>
                        <div className="hover-title-box padding-55px-lr width-300px sm-width-100 sm-padding-20px-lr">
                            <div className="separator width-50px bg-black md-display-none xs-margin-lr-auto"></div>
                            <h3><a className="text-white font-weight-600 alt-font text-white-hover" href="single-project-page-05.html">{project.introduction}</a></h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



export default SingleProject;