
import React from 'react';


class SelfIntro extends React.Component {
    resumeRender = () => {
        const { resumeRender, showResume } = this.props;
        let resumeShow = !showResume;
        resumeRender(resumeShow);
    }


    blogOpen = (e) => {
        e.stopPropagation() 
        const href = "https://stackoverflow.com/questions/17807929/javascript-get-href-onclick"
        window.open(href);
        return false;
    }



    render() {
        const { showResume } = this.props;
        return (
            <div className="swiper-slide width-550px xs-width-100 xs-height-auto">
                <div className="position-relative width-90 height-100 display-table padding-ten-all xs-padding-fifteen-all xs-width-100">
                    <div className="display-table-cell vertical-align-middle">
                        <h4 className="text-medium-gray display-block margin-5px-bottom alt-font">Hello,</h4>
                        <h6 className="text-medium-gray font-weight-300 margin-20px-bottom alt-font">I'm Xi Zhang</h6>
                        <p className="text-large dispaly-block pull-left font-weight-300 width-90 margin-35px-bottom">I design thoughtful digital experiences & beautiful brand aesthetics. I provide high quality web design services.</p>
                        {
                        showResume ? (<span className="text-medium-gray font-weight-300 margin-20px-bottom alt-font" id="selfIntro-projects" onClick={this.resumeRender}> Projects </span>) : (                        <span className="text-medium-gray font-weight-300 margin-20px-bottom alt-font" id="selfIntro-resume" onClick={this.resumeRender}> Resume </span>
)
                        }
                        <span className="text-medium-gray font-weight-300 margin-20px-bottom alt-font"> <a href="#dsa" id="selfIntro-blog" onClick={(e) => this.blogOpen(e)}>Blog</a> </span>


                    </div>
                </div>
            </div>
        );
    }
}


export default SelfIntro;